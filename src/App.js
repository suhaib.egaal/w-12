import React from 'react';

function Person() {
  return (
    <div className="person">
      <div>
        <img src="http://www.cc.puv.fi/~tka/vamk.png" alt="VAMK Logo" />
        <h2 className="bc">Suhaib Egal</h2>
        <p></p>
        <span>Opiskelija</span><br />
        <span>Tietotekniikka</span><br />
        <p></p>
        <br />
        <span>e2203603@edu.vamk.fi</span>
        <br />
        <span>+358 41 313 3397</span>
        <p></p>
        <span>Wolffintie 30, FI-65200 VAASA, Finland</span>
      </div>
    </div>
  );
}
export default Person; 